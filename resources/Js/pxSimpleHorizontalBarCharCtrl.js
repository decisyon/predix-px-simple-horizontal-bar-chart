(function() {
	'use strict';
	
	/**
	* Controller to manage the Simple Horizontal bar chart widget.
	* link:https://www.predix-ui.com/#/components/px-simple-horizontal-bar-chart/
	*/
	function PxSimpleHorizontalBarChartCtrl($scope, $timeout, $element)
	{
		var ctrl = this,
			ctx = $scope.DECISYON.target.content.ctx,
			refObjId = ctx.instanceReference.value,
			target = $scope.DECISYON.target,
			unbindWatches = [],
			DEFAULT_BAR_LABELS = 'percentage',
			OVERFLOW_BUG_FIX = 3;
		
		/* Return the size input after adjusting its value by removing % and space white.
		The width and the height are adjusted because the Predix component doesn't supported values in percentage
		*/
		var getAdjustedSizing = function(size) {
			var adjustedSize = size.replace (/%/g, '').trim();
			return (parseInt(adjustedSize)) ? parseInt(adjustedSize) : '';
		};
		
		/* Set the container widget size. For height, it needs to detract 3px to not show unnecessary scrolling bar
		If you set both this and height to "auto", the chart will expand to fill its containing element.
		Note: The parent containing element must be a block-level element or have a defined width/height so that the component can inherit the value.
		See: GItHub documentation https://github.com/PredixDev/px-simple-horizontal-bar-chart
		*/
		var setContainerSizing = function(context) {
			var height = getAdjustedSizing(context['min-height'].value);
			ctrl.width = getAdjustedSizing(context['min-width'].value);
			ctrl.height = (height !== '' && height > OVERFLOW_BUG_FIX) ? (height - OVERFLOW_BUG_FIX) : height;
		};
		
		/* Management of chart properties by reading the context */
		var manageParamsFromContext = function(context) {
			setContainerSizing(context);
			ctrl.barLabels = (context.$barLabels.value !== '') ? context.$barLabels.value.value : DEFAULT_BAR_LABELS;
		};
		
		/* Set chart data*/
		var setChartData = function(data) {
			ctrl.chartData = data.chartData;
			ctrl.legendLabels = data.legendLabels;
			ctrl.currentTpl = ctrl.chartView;
		};
		
		/* Set the view in error */
		var setChartInError = function(rejection) {
			ctrl.errorMsg = rejection.errorMsg;
			ctrl.currentTpl = ctrl.errorView;
			destroyWidget();
		};
		
		/* Set the callback for data connector*/
		var setCallbackDC = function(dataConnector) {
			dataConnector.instance.data.then(function success(data) {
				setChartData(data[0]);
			},function error(rejection) {
				setChartInError(rejection);
			});
		};
		
		/* Get data from the associated data connector*/
		var getChartData = function() {
			var dc, dataChart,
				useDataConnector = ctx.useDataConnector.value;
			if (useDataConnector) {
				if (target.getDataConnector) {
					dc = target.getDataConnector();
					dc.ready(function(dataConnector) {
						dataConnector.onUpdate(function(data) {
							setCallbackDC(dataConnector);
						});
						setCallbackDC(dataConnector);
					});
				} else {
					setChartInError({errorMsg : 'The widget can\'t be inizialized because it needs to be associated to a data connector.'});
					ctrl.chartData = [];
					ctrl.legendLabels = [];
				}
			} else {
				setChartInError({errorMsg : 'The Use data connector property is set to false.'});
			}
		};
		
		/* Destroy the widget reference*/
		var destroyWidget = function() {
			var i = 0;
			for (i; i < unbindWatches.length; i++){
				unbindWatches[i]();
			}
		};
		
		/* Watch on widget context to observe parameter changing after loading */
		var attachListeners = function() {
			unbindWatches.push($scope.$watch('DECISYON.target.content.ctx', function(newCtx, oldCtx) {
				if (!angular.equals(newCtx, oldCtx)){
					ctrl.manageParamsFromContext(newValue.ctx);
				}
			}, true));
			/* Listener on destroy angular element; in this case we destroy all watch*/
			$scope.$on('$destroy', destroyWidget);
		};
		
		/* Initialize */
		var inizializeChart = function() {
			ctrl.widgetID = 'pxSimpleHorizontalBarChart_' + refObjId;
			ctrl.chartView = 'simpleHorizontalBarChart.html';
			ctrl.errorView = 'error.html';
			getChartData();
			manageParamsFromContext(ctx);
			attachListeners();
		};
	
		inizializeChart();
	}
	PxSimpleHorizontalBarChartCtrl.$inject = ['$scope', '$timeout', '$element'];
	
	DECISYON.ng.register.controller('pxSimpleHorizontalBarChartCtrl', PxSimpleHorizontalBarChartCtrl);
}());